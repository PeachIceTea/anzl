from flask import Flask


def create_app(config):
    app = Flask(__name__, template_folder="templates", static_folder="static")
    app.config.from_mapping(
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SQLALCHEMY_DATABASE_URI="sqlite:///anzl.db",
        SECRET_KEY=b"\x97\xce\x8c\xe8\xb1 $\x80i\xb2,\xbaR\x01\xb4\x9a\xe5\xb7\x9dte\x1c\x0e\t",
    )
    app.config.from_object(config)

    # Setup orm
    from .db import db

    db.init_app(app)

    # Setup migrations
    from .alembic import alembic

    alembic.init_app(app)

    # Setup routes
    from .controllers import register_blueprints

    register_blueprints(app)

    return app

