from anzl.controllers.index import bp_index


def register_blueprints(app):
    app.register_blueprint(bp_index)

