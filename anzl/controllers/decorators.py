from flask import session, flash, redirect
from functools import wraps

from anzl.models.user import UserType


def login_required(f):
    """Checks for any 
    
    :param f: [description]
    :type f: [type]
    :return: [description]
    :rtype: [type]
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("userid") is None:
            # TODO: redirect to the requested page after user logged in
            flash("Sie müssen eingeloggt sein, um diese Seite aufzurufen.")
            return redirect("/login")

        return f(*args, **kwargs)

    return decorated_function


def type_required(usertype):
    def returned_function(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            session_type = session.get("type")
            if session_type is None:
                flash("Sie müssen eingeloggt sein, um diese Seite aufzurufen.")
                return redirect("/login")

            session_type = UserType(session_type)
            if session_type != usertype:
                flash("Sie haben keine berechtigung diese Seite aufzurufen.")
                return redirect("/")

            return f(*args, **kwargs)

        return decorated_function

    return returned_function
