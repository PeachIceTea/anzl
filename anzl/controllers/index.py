from flask import Blueprint, render_template, request, flash, redirect, session
from sqlalchemy import exc

from anzl.db import db
from anzl.controllers.decorators import login_required
from anzl.models.user import User

bp_index = Blueprint("bp_index", __name__)


@bp_index.route("/")
def index():
    return render_template("index.html")


@bp_index.route("/create", methods=("GET", "POST"))
def create_user():
    """Creates new User

    Renders a form for GET requests.
    POST Requests either redirect to the main page if the creation was 
    successful or flashes any errors above the form.

    TODO: Enforce a minimum length on inputs.
    TODO: Enforce a password policy that adhears to BSI standards.

    We do not test whether the username is unique, instead we catch an the
    exception if it isn't. 

    :return: Redirects to main page on success. Flashes errors on error.
    :rtype: flask.Response
    """
    if request.method == "GET":
        return render_template("create.html")

    username = request.form["username"]
    firstname = request.form["fname"]
    lastname = request.form["lname"]
    password = request.form["password"]

    error = False

    if not username:
        flash("Nutzername fehlt.")
        error = True

    if not firstname:
        flash("Vorname fehlt.")
        error = True

    if not lastname:
        flash("Name fehlt.")
        error = True

    if not password:
        flash("Password fehlt.")
        error = True

    if error:
        return render_template("create.html")

    user = User(
        username=username, firstname=firstname, lastname=lastname, password=password
    )

    user.normalize_names()

    db.session.add(user)

    try:
        db.session.commit()
    except exc.IntegrityError:
        """Catches exception thrown when the given username is already in use

        In theory an IntegrityError is an "Exception raised when the relational
        integrity of the database is affected, e.g. a foreign key check fails,
        duplicate key, etc."
        However the only reason it should be thrown in this function is if the
        username already exists in the database, as there are no other
        constraints that we do not test for.        
        """
        flash("Nutzername ist bereits in Verwendung.")
        return render_template("create.html")
    except Exception as e:
        # Should only be thrown if we cannot connect to the database
        print(e)
        flash(
            "Unbekannter Fehler ist aufgetreten. Bitte versuchen Sie es erneut. Sollte der Fehler weiterhin bestehen, kontaktieren Sie bitte die Administratoren."
        )
        return render_template("create.html")

    return redirect("/login")


@bp_index.route("/login", methods=("GET", "POST"))
def login():
    """Login for users
    
    :return: Redirects to secure page on success. Flashes on error.
    :rtype: flask.Response
    """
    if session.get("userid") is not None:
        flash("Sie sind bereits eingeloggt.")
        return redirect("/secure")

    if request.method == "GET":
        return render_template("login.html")

    username = request.form["username"]
    password = request.form["password"]

    error = False

    if not username:
        flash("Nutzername fehlt.")
        error = True

    if not password:
        flash("Password fehlt.")
        error = True

    if error:
        return render_template("login.html")

    user = User.query.filter_by(username=username).first()
    if user is None:
        flash("Benutzer nicht gefunden.")
        return render_template("login.html")

    if not user.test_password(password):
        flash("Das eingegebene Passwort stimmt nicht überein.")
        return render_template("login.html")

    session["userid"] = user.id
    session["name"] = f"{user.firstname} {user.lastname}"
    session["type"] = user.type.value

    return redirect("/secure")


@bp_index.route("/secure")
@login_required
def secure():
    """Test page for logged in users
    
    :return: A test page on success. Redirect to login page on error.
    :rtype: flask.Response
    """
    return render_template("secure.html")


@bp_index.route("/logout", methods=("POST",))
def logout():
    """Clears user session
    
    :return: Redirect to login page
    :rtype: flask.Response
    """
    if session.get("userid") is not None:
        session.clear()
        flash("Sie wurden ausgeloggt.")
    return redirect("/login")
