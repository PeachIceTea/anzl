import enum
from sqlalchemy import Column, Integer, String, CHAR, event, Enum, ForeignKey

from anzl.db import db


class Salutation(enum.Enum):
    Herr = 0
    Frau = 0


class Applicant(db.Model):
    __tablename__ = "applicant"

    id = Column(Integer, primary_key=True)
    username = Column(String(50), nullable=False, unique=True)
    password = Column(CHAR(60), nullable=False)

    email = Column(String(100), unique=True)
    phone = Column(String(50), unique=True)
    fax = Column(String(50), unique=True)

    firstname = Column(String(25), nullable=False)
    lastname = Column(String(25), nullable=False)
    street = Column(String(50), nullable=False)
    zipcode = Column(String(15), nullable=False)
    city = Column(String(50), nullable=False)
    company = Column(String(100))

    user = Column(Integer, ForeignKey("user.id"), nullable=False, unique=True)
