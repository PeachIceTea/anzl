import enum
from sqlalchemy import Column, Integer, String, CHAR, event, Enum
from sqlalchemy.orm import relationship
import bcrypt

from anzl.db import db
from anzl.models.applicant import Applicant


class UserType(enum.Enum):
    Applicant = 0
    Clerk = 1
    Admin = 2


class User(db.Model):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    type = Column(Enum(UserType), nullable=False, default=UserType.Applicant)

    username = Column(String(50), nullable=False, unique=True)
    password = Column(CHAR(60), nullable=False)

    def __repr__(self):
        return f"<User(id='{self.id}', username='{self.username}')"

    def normalize_names(self):
        """Normalizes names to be consistent across the database
        """
        self.username = self.username.lower()
        self.firstname = self.firstname.title()
        self.lastname = self.lastname.title()

    def hash_password(self):
        """Hashes the user password
        """
        self.password = bcrypt.hashpw(
            str.encode(self.password), bcrypt.gensalt(rounds=11)
        )

    def test_password(self, password):
        """Test input password against stored password hash
        
        :param password: Password entered by the
        :type password: string
        :return: Whether the password matches
        :rtype: boolean
        """
        return bcrypt.checkpw(str.encode(password), self.password)


@event.listens_for(User, "before_insert")
def hash_password(mapper, connection, target):
    target.hash_password()


@event.listens_for(User, "before_insert")
def normalize_names(mapper, connection, target):
    target.normalize_names()
